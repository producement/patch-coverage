FROM alpine:latest

RUN apk update && apk add bc bash git libxml2-utils diffutils

RUN mkdir /app
WORKDIR /app

ADD src/patchcov /usr/local/bin
ADD src/changed_lines_difftool /usr/local/bin
RUN chmod +x /usr/local/bin/patchcov
RUN chmod +x /usr/local/bin/changed_lines_difftool

CMD ["/usr/local/bin/patchcov"]
