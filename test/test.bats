#!/usr/bin/env bats

setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
    # make executables in src/ visible to PATH
    PATH="$DIR/../src:$PATH"
    source patchcov
}

function assert_number {
  assert_output "$(echo $@ | xargs printf '%.2f\n' | xargs)"
}

@test "get_average calculates average of 1,2,3" {
  run get_average 1 2 3
  assert_number '2.00'
}

@test "get_average supports floating point numbers" {
  run get_average 1 0
  assert_number '0.50'
}

@test "get_average of 0 is 0" {
  run get_average 0
  assert_number '0.00'
}

@test "get_average of empty is 1.00" {
  run get_average
  assert_number '1.00'
}

@test "get_changed_lines_difftool" {
  export BASE="com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt"
  run src/changed_lines_difftool "$DIR/sources/first.source" "$DIR/sources/second.source"
  assert_output 'com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt:2,3'
}

@test "get_line_coverage - cobertura - not covered file is not reported" {
  run get_line_coverage "some/file" "1" "$DIR/reports/cobertura.xml"
  assert_output ''
}

@test "get_line_coverage - cobertura - existing line" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "34" "$DIR/reports/cobertura.xml" "src/main/kotlin"
  assert_number '1.00'
}

@test "get_line_coverage - cobertura  - existing line low coverage" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "87" "$DIR/reports/cobertura.xml" "src/main/kotlin"
  assert_number '0.00'
}

@test "get_line_coverage - cobertura  - existing line partial coverage" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "80" "$DIR/reports/cobertura.xml" "src/main/kotlin"
  assert_number '0.50'
}

@test "get_line_coverage - cobertura - should work without package" {
  run get_line_coverage "patch-coverage" "151" "$DIR/reports/cobertura2.xml" "/home/maidok/Development/producement/patch-coverage/src"
  assert_number '1.00'
}

@test "get_line_coverage - unknown - unknown report" {
  run get_line_coverage "some/file" "1" "$DIR/reports/unknown.xml"
  assert_output 'Unknown report type dummy.'
  assert_failure
}

@test "get_line_coverage - jacoco - not covered file is not reported" {
  run get_line_coverage "some/file" "1" "$DIR/reports/jacocoTestReport.xml"
  assert_output ''
}

@test "get_line_coverage - jacoco - existing line" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "34" "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin"
  assert_number '1.00'
}

@test "get_line_coverage - jacoco  - existing line low coverage" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "87" "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin"
  assert_number '0.00'
}

@test "get_line_coverage - jacoco  - existing line partial coverage" {
  run get_line_coverage "com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "80" "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin"
  assert_number '0.50'
}

@test "get_line_coverage - jacoco - existing line, ignores prefix" {
  run get_line_coverage "src/main/kotlin/com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt" "34" "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin"
  assert_number '1.00'
}

@test "calculate_coverages" {
  run calculate_coverages "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin" "src/main/kotlin/com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt:36"
  assert_number '1.00'
}

@test "calculate_coverages - not covered files are ignored and dont report coverage" {
  run calculate_coverages "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin" "src/main/kotlin/com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt:36" ".gitlab-ci.yml:20"
  assert_number '1.00'
}

@test "calculate_coverages - not covered files are ignored and dont report coverage, reverse order" {
  run calculate_coverages "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin" ".gitlab-ci.yml:20" "src/main/kotlin/com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt:36"
  assert_number '1.00'
}

@test "calculate_coverages - multiple lines per file" {
  run calculate_coverages "$DIR/reports/jacocoTestReport.xml" "src/main/kotlin" "src/main/kotlin/com/autobrand/socialAuthentication/facebook/FacebookGraphApiService.kt:34,87,80"
  assert_number '1.00' '0.00' '0.50'
}

@test "debug log doesnt do anything when DEBUG not defined" {
  run debug "Hello"
  assert_output ''
}

@test "debug log works when DEBUG is defined" {
  export DEBUG=true
  run debug "Hello"
  assert_output 'Hello'
}

@test "main - fails when report file is missing" {
  run main
  assert_output 'build/reports/jacoco/test/jacocoTestReport.xml does not exist.'
  assert_failure
}
