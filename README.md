# Patch Coverage

Calculates Git commit patch coverage from a test report. Currently supports Jacoco and Cobertura.

## Usage

Run `src/patch-coverage` with the following arguments in order:
- Current branch (Defaults to `git rev-parse --abbrev-ref HEAD`)
- Base branch (Defaults to `main`)
- Source path. Removed from file names to match with names in test reports (Defaults to `src/main/kotlin`)
- Test report path (Defaults to `build/reports/jacoco/test/jacocoTestReport.xml`)

Outputs a single number with current patch coverage as a decimal

## Docker

Also available as a Docker image at `registry.gitlab.com/producement/patch-coverage:latest`
